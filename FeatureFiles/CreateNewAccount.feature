﻿Feature: CreateNewAccount
	
@newaccount
Scenario Outline: Create Demoblaze New Account
	Given the user navigates to Demoblaze website
	Then the user is landing in homepage successfully
	And the user signup for new account '<username>' and '<password>'
	And the new account should be created succesfully

Examples: 
| username   | password   | 
| omuhle5           | omuhlepassword5           | 