﻿Feature: AddProduct2Cart
	
@addproduct
Scenario Outline: Add Product to Cart
	Given the user navigates to Demoblaze website
	Then the user is landing in homepage successfully
	And the user login to the website '<username>' and '<password>' 
	And the user add product to cart
	And the product added to cart successfully

Examples: 
| username            | password                  | 
| omuhle123           | omuhlepassword3           | 