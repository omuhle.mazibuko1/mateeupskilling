﻿Feature: PlaceOrderANDvalidateOrderWasSuccesful

@placeorder
Scenario Outline: User Place an Order
	Given the user navigates to Demoblaze website
	Then the user is landing in homepage successfully
	And the user add product to cart
	And the product added to cart successfully
	And the user navigates to cart
	And the user should be navigated to cart successfully
	And the user place order '<name>'  and '<country>' and '<city>' and '<creditcard>' and '<month>' and '<year>'
	And the order should be successfully

Examples: 
| name               | country                      | city                          | creditcard     | month             | year     |
| omuhle123          |  south african               |  johannesburg                 | 11111           |  June             |  2021    |