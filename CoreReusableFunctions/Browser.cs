﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Chrome;
using System.Drawing;

namespace WhereIsMyTransportAssessment.CoreReusableFunctions
{
    class Browser
    {
        [Obsolete]
        public static IWebDriver Current
        {
            get
            {
                if (!FeatureContext.Current.ContainsKey("browser"))
                {
                    IWebDriver driver = null;

                    var proxy = new Proxy { IsAutoDetect = true };
                    driver = new ChromeDriver();

                    FeatureContext.Current["browser"] = driver;

                }

                return (IWebDriver)FeatureContext.Current["browser"];
            }
        }

        [Obsolete]
        public static void PressDownArrowKey()
        {
            int count = 0;
            Actions act = new Actions(Current);
            while (count <= 5)
            {
                act.SendKeys(Keys.PageDown);
                Console.WriteLine("Number of times the Down Arrow Key Pressed:" + "" + count);
                count++;
            }
        }

           public static IWebElement WaitForElement(By by, int timeoutSeconds = 60)
           {
            var wait = new WebDriverWait(Current, TimeSpan.FromSeconds(timeoutSeconds));
            IWebElement ExpectedElement = Current.FindElement(by);
            return ExpectedElement;
           }

        [Obsolete]
        public static void switchToPopUp()
        {
            Current.SwitchTo().ActiveElement();
            Console.WriteLine("WebDriver has switched succesfully to the active popup...");
        }

        [Obsolete]
        public static void CloseSession()
        {
            Current.Close();
            Console.WriteLine("Selenium WebDriver seesion has been Ended...");
        }

        [Obsolete]
        public static void AcceptPopup()
        {
            Current.SwitchTo().Alert().Accept();
            Console.WriteLine("Pop up Accepted...");
        }

        [Obsolete]
        public static void RefreshPage()
        {
            Current.Navigate().Refresh();
            Console.WriteLine("Page has Been refreshed successfully...");
        }

        [Obsolete]
        public static T NavigateToPage<T>() where T : BasePage, new()
        {
            var page = new T();
            page.OnPageLoad();

            Current.Navigate().GoToUrl(page.URL);

            try
            {
                if (page.IsValid())
                {
                    FeatureContext.Current["currentPage"] = page;
                    return page;
                }
            }
            catch { }

            Console.WriteLine("Attempt 2 at navigate");

            Current.Navigate().GoToUrl(page.URL);

            try
            {
                if (page.IsValid())
                {
                    FeatureContext.Current["currentPage"] = page;
                    return page;
                }
            }
            catch { }

            Console.WriteLine("Attempt 3 at navigate");

            var proxy = new Proxy { IsAutoDetect = true };

            IWebDriver driver = new ChromeDriver();

            FeatureContext.Current["browser"] = driver;

            Current.Manage().Window.Position = new Point(0, 0);
            Current.Manage().Window.Maximize();

            Current.Navigate().GoToUrl(page.URL);

            if (page.IsValid())
            {
                FeatureContext.Current["currentPage"] = page;
                return page;
            }

            return page;
        }

    }
}

