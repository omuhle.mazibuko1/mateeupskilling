﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WhereIsMyTransportAssessment.CoreReusableFunctions
{
   public abstract class BrowserablePage
    {
        public virtual string URL { get { return string.Empty; } }
    }
}
