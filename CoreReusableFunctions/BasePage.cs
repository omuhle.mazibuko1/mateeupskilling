﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace WhereIsMyTransportAssessment.CoreReusableFunctions
{
   public abstract class BasePage : BrowserablePage
    {
        [Obsolete]
        public string Title
        {
            get
            {
                return Browser.Current.Title;
            }
        }

        [Obsolete]
        public virtual void OnPageLoad()
        {
            Browser.Current.Manage().Window.Position = new Point(0, 0);
            Browser.Current.Manage().Window.Maximize();
        }

        public abstract bool IsValid();

    }
}
