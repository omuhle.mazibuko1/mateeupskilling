﻿using System;
using TechTalk.SpecFlow;
using WhereIsMyTransportAssessment.CoreReusableFunctions;
using WhereIsMyTransportAssessment.PageFunctions;
using WhereIsMyTransportAssessment.PageObjects;

namespace WhereIsMyTransportAssessment.StepDefinitions
{
    [Binding]
    public class CreateNewAccountSteps
    {
        DemoblazeHomePageFunctions homepageObj = new DemoblazeHomePageFunctions();
        CreateNewAccountPageFunctions newaccountObj = new CreateNewAccountPageFunctions();

        [Given(@"the user navigates to Demoblaze website")]
        [Obsolete]
        public void GivenTheUserNavigatesToDemoblazeWebsite()
        {
            var homepage = Browser.NavigateToPage<DemoblazeHomePage>();
            homepage.IsValid().Equals(true);
        }
        
        [Then(@"the user is landing in homepage successfully")]
        public void ThenTheUserIsLandingInHomepageSuccessfully()
        {
            homepageObj.ValidateDemoblazeHomePage();
        }
        
        
     

        [Then(@"the user signup for new account '(.*)' and '(.*)'")]
        [Obsolete]
        public void ThenTheUserSignupForNewAccountAnd(string param1, string param2)
        {
            newaccountObj.createNewAccount(param1, param2);
        }



        [Then(@"the new account should be created succesfully")]
        [Obsolete]
        public void ThenTheNewAccountShouldBeCreatedSuccesfully()
        {
            newaccountObj.confirmNewAccountCreation();
        }
    }
}
