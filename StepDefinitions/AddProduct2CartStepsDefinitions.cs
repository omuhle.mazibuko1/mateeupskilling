﻿using System;
using TechTalk.SpecFlow;
using WhereIsMyTransportAssessment.PageFunctions;

namespace WhereIsMyTransportAssessment.StepDefinitions
{
    [Binding]
    public class AddProduct2CartSteps
    {
        AddProduct2CartPageFunctions cart = new AddProduct2CartPageFunctions();

        [Then(@"the user add product to cart")]
        public void ThenTheUserAddProductToCart()
        {
            cart.SelectProduct();
            cart.AddProdct2Cart();
        }
        
        [Then(@"the product added to cart successfully")]
        public void ThenTheProductAddedToCartSuccessfully()
        {
            cart.validateProductAdded();
        }
    }
}
