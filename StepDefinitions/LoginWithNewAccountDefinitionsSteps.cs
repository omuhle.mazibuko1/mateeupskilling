﻿using System;
using TechTalk.SpecFlow;
using WhereIsMyTransportAssessment.PageFunctions;

namespace WhereIsMyTransportAssessment.StepDefinitions
{
    [Binding]
    public class LoginWithNewAccountSteps
    {
        LoginWithNewAccountPageFunctions newaccountObj = new LoginWithNewAccountPageFunctions();

        [Then(@"the user login to the website '(.*)' and '(.*)'")]
        public void ThenTheUserLoginToTheWebsiteAnd(string user, string pass)
        {
            newaccountObj.userlogin(user,pass);
        }


        [Then(@"the user should be logged in succesfully (.*)")]
        public void ThenTheUserShouldBeLoggedInSuccesfully(string username)
        {
            newaccountObj.validateUserLoggedIn(username);
           
        }

        [Then(@"the user user logsout the Demoblaze website")]
        public void ThenTheUserUserLogsoutTheDemoblazeWebsite()
        {
            newaccountObj.CloseDemoblazeWebsite(); ;
        }

    }
}
