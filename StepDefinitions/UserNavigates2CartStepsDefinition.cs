﻿using System;
using TechTalk.SpecFlow;
using WhereIsMyTransportAssessment.PageFunctions;

namespace WhereIsMyTransportAssessment.StepDefinitions
{
    [Binding]
    public class UserNavigates2CartSteps
    {
        UserNavigates2CartPageFunctions cartfunct = new UserNavigates2CartPageFunctions();

        [Then(@"the user navigates to cart")]
        public void ThenTheUserNavigatesToCart()
        {
            cartfunct.clickCartMenu();
        }
        
        [Then(@"the user should be navigated to cart successfully")]
        public void ThenTheUserShouldBeNavigatedToCartSuccessfully()
        {
            cartfunct.validateUserLandedOnProductCartPage();
        }
    }
}
