﻿using System;
using TechTalk.SpecFlow;
using WhereIsMyTransportAssessment.PageFunctions;

namespace WhereIsMyTransportAssessment.StepDefinitions
{
    [Binding]
    public class PlaceOrderANDvalidateOrderWasSuccesfulSteps
    {
        PlaceOrderANDvalidateOrderWasSuccesfulPageFunctions functs = new PlaceOrderANDvalidateOrderWasSuccesfulPageFunctions();


        [Then(@"the user place order '(.*)'  and '(.*)' and '(.*)' and '(.*)' and '(.*)' and '(.*)'")]
        [Obsolete]
        public void ThenTheUserPlaceOrderAndAndAndAndAnd(string name, string country, string city, string creditc, string month, string yr)
        {
            functs.StartOrder(name, country, city, creditc, month, yr);
        }

        [Then(@"the order should be successfully")]
        public void ThenTheOrderShouldBeSuccessfully()
        {
            Console.WriteLine("The Order should be successfully created...");
        }
    }
}
