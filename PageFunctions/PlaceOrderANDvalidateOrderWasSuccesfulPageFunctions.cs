﻿using System;
using System.Collections.Generic;
using System.Text;
using WhereIsMyTransportAssessment.CoreReusableFunctions;
using WhereIsMyTransportAssessment.PageObjects;

namespace WhereIsMyTransportAssessment.PageFunctions
{
    class PlaceOrderANDvalidateOrderWasSuccesfulPageFunctions : PlaceOrderANDvalidateOrderWasSuccesfulPage
    {
        [Obsolete]
        public void StartOrder(string name,string country,string city,string creditcard,string month,string year)
        {
            System.Threading.Thread.Sleep(1000);
            Browser.WaitForElement(getPlaceOrderbtn()).Click();
            Browser.switchToPopUp();
            System.Threading.Thread.Sleep(1000);

            Browser.WaitForElement(getNamefiled()).SendKeys(name);
            Browser.WaitForElement(getCountryfield()).SendKeys(country);
            Browser.WaitForElement(getCityfield()).SendKeys(city);
            Browser.WaitForElement(getCreditCardfield()).SendKeys(creditcard);
            Browser.WaitForElement(getMonthfield()).SendKeys(month);
            Browser.WaitForElement(getYearfield()).SendKeys(year);
            System.Threading.Thread.Sleep(1000);
            Browser.PressDownArrowKey();
            Browser.WaitForElement(getPurchasebtn()).Click();
        }
    }
}
