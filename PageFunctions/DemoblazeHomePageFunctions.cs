﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using WhereIsMyTransportAssessment.CoreReusableFunctions;
using WhereIsMyTransportAssessment.PageObjects;

namespace WhereIsMyTransportAssessment.PageFunctions
{
    class DemoblazeHomePageFunctions : DemoblazeHomePage
    {

        public void ValidateDemoblazeHomePage()
        {
            if (Browser.WaitForElement(getHomeMenu()).Displayed == true)
            {
                Console.WriteLine("User has landed succesfully on DemoBlaze");
            }
            else 
            {
                Assert.Fail("Demoblaze could not be loaded",false);
            }
        }
    }
}
