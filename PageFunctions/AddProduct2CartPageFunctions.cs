﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using WhereIsMyTransportAssessment.CoreReusableFunctions;
using WhereIsMyTransportAssessment.PageObjects;

namespace WhereIsMyTransportAssessment.PageFunctions
{
    class AddProduct2CartPageFunctions : AddProduct2CartPage
    {
        public void SelectProduct()
        {
            System.Threading.Thread.Sleep(2000);
            bool itemfound = Browser.WaitForElement(getSpecificItem()).Displayed;
            if (itemfound == true)
            {
                System.Threading.Thread.Sleep(3000);
                Browser.WaitForElement(getSpecificItem()).Click();
                Console.WriteLine("Product was Selected succesfully...");
            }
            else
            {
                Assert.Fail("Element NOT Found To Select...", itemfound);
            }
        }

        public void AddProdct2Cart()
        {
            System.Threading.Thread.Sleep(3000);
            bool add2CatbuttonVisible = Browser.WaitForElement(getAdd2Cartbtn()).Displayed;
            if (add2CatbuttonVisible == true)
            {
                System.Threading.Thread.Sleep(3000);
                Browser.WaitForElement(getAdd2Cartbtn()).Click();
                Console.WriteLine("Product was added to the Cart Succesfully...");
            }
            else
            {
                Assert.Fail("Add to Cart button was NOT found...", add2CatbuttonVisible);
            }
        }
        public void clickCartMenu()
        {
            System.Threading.Thread.Sleep(3000);
            Browser.WaitForElement(getCartmenu()).Click();

        }

        [Obsolete]
        public void validateProductAdded()
        {
            System.Threading.Thread.Sleep(2000);
              Browser.AcceptPopup();
        }
    }
}
