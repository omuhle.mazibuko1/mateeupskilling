﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using WhereIsMyTransportAssessment.CoreReusableFunctions;
using WhereIsMyTransportAssessment.PageObjects;

namespace WhereIsMyTransportAssessment.PageFunctions
{
    class LoginWithNewAccountPageFunctions : LoginWithNewAccountPage
    {
        [Obsolete]
        public void userlogin(string username, string password)
        {
            Browser.WaitForElement(getLoginLink()).Click();
            System.Threading.Thread.Sleep(100);
            Browser.switchToPopUp();
            Browser.WaitForElement(getUsername()).SendKeys(username);
            Browser.WaitForElement(getPassword()).SendKeys(password);
            Browser.WaitForElement(getLoginbutton()).Click();
        }

        public void validateUserLoggedIn(string username)
        {
            bool userloggedIn = Browser.WaitForElement(getloggedInUsername()).Displayed;
            if (userloggedIn == true)
            {
                var actualusernameText = Browser.WaitForElement(getloggedInUsername()).Text;
                if (actualusernameText == username.ToString().Trim())
                {
                    Console.WriteLine("User has logged In Successfully....");
                    System.Threading.Thread.Sleep(3000);
                }
                else
                {
                    Assert.Fail("User was Not able to login Successfully....",false);
                }
            }
        }

        [Obsolete]
        public void CloseDemoblazeWebsite()
        {

            Browser.CloseSession();

        }
    }
}
