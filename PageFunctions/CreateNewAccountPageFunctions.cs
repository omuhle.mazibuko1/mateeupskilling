﻿using System;
using System.Collections.Generic;
using System.Text;
using WhereIsMyTransportAssessment.CoreReusableFunctions;
using WhereIsMyTransportAssessment.PageObjects;

namespace WhereIsMyTransportAssessment.PageFunctions
{
    class CreateNewAccountPageFunctions : CreateNewAccountPage
    {
        [Obsolete]
        public void createNewAccount(string username,string password)
        {
            Browser.WaitForElement(getSignUpLink()).Click();
            Browser.switchToPopUp();
            System.Threading.Thread.Sleep(100);
            Browser.WaitForElement(getSignUpusernamefield()).SendKeys(username);
            Browser.WaitForElement(getSignUppasswordfield()).SendKeys(password);
            Browser.WaitForElement(getSignUpbutton()).Click();  
        }

        [Obsolete]
        public void confirmNewAccountCreation()
        {
            System.Threading.Thread.Sleep(1000);
            Browser.AcceptPopup();
        }
    }
}
