﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using WhereIsMyTransportAssessment.CoreReusableFunctions;
using WhereIsMyTransportAssessment.PageObjects;

namespace WhereIsMyTransportAssessment.PageFunctions
{
    class UserNavigates2CartPageFunctions : UserNavigates2CartPage
    {
        public void clickCartMenu()
        {
            Browser.WaitForElement(getCartMenuLink()).Click();
            System.Threading.Thread.Sleep(3000);
        }
        public bool validateUserLandedOnProductCartPage()
        {
            bool userOnCartPage = Browser.WaitForElement(getPlaceOrderbtn()).Displayed;
            if (userOnCartPage == true)
            {
                Console.WriteLine("User Has Landed Succesfully On Cart Page");
            }
            else
            {
                userOnCartPage = false;
                Assert.Fail("User was Unable to Land Succesfully On Cart Page",userOnCartPage);
            }
            return true;
        }

       
    }
}
