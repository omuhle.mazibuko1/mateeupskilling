﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace WhereIsMyTransportAssessment.PageObjects
{
    class PlaceOrderANDvalidateOrderWasSuccesfulPage
    {
       public By getPlaceOrderbtn()
        {
            By orderbtn = By.XPath("//*[@id=\"page-wrapper\"]/div/div[2]/button");
            
            return orderbtn;
        }

        public By getNamefiled()
        {
            By namefield = By.Id("name");
            return namefield;
        }

        public By getCountryfield()
        {
            By country = By.Id("country");
            return country;
        }
        public By getCityfield()
        {
            By city = By.Id("city");
            return city;
        }

        public By getCreditCardfield()
        {
            By creditcard = By.Id("card");
            return creditcard;
        }

        public By getYearfield()
        {
            By year = By.Id("year");
            return year;
        }

        public By getMonthfield()
        {
            By monthfield = By.Id("month");
            return monthfield;
        }

        public By getPurchasebtn()
        {
            By purchasebtn = By.XPath("//*[@id=\"orderModal\"]/div/div/div[3]/button[2]");
            return purchasebtn;
        }
    }
}
