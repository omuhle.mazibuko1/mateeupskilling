﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace WhereIsMyTransportAssessment.PageObjects
{
    class UserNavigates2CartPage
    {
        public By getCartMenuLink()
        {
            By menulink = By.XPath("//*[@id=\"navbarExample\"]/ul/li[4]/a");
            return menulink;
        }

        public By getPlaceOrderbtn()
        {
            By orderbtn = By.XPath("//*[@id=\"page-wrapper\"]/div/div[2]/button");
            
            return orderbtn;
        }
    }
}
