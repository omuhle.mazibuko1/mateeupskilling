﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using WhereIsMyTransportAssessment.CoreReusableFunctions;

namespace WhereIsMyTransportAssessment.PageObjects
{
    class DemoblazeHomePage : BasePage
    {
      
        private readonly string _url = "https://demoblaze.com/";
    
        public override string URL
        {
            get
            {
                return _url;
            }
        }

        public override bool IsValid()
        {
            return Browser.WaitForElement(getHomeMenu(), 120) != null;
        }

        public By getHomeMenu()
        {
            By homemenu = By.XPath("//*[@id=\"navbarExample\"]/ul/li[1]/a");
            return homemenu;
        }
    }
}
