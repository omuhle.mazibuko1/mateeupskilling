﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace WhereIsMyTransportAssessment.PageObjects
{
    class CreateNewAccountPage 
    {
        public By getSignUpLink()
        {
            By signuplink = By.Id("signin2");
            return signuplink;
        }

        public By getSignUpusernamefield()
        {
            By username = By.Id("sign-username");
            return username;
        }

        public By getSignUppasswordfield()
        {
            By password = By.Id("sign-password");
            return password;
        }

        public By getSignUpbutton()
        {
            By button = By.XPath("//*[@id=\"signInModal\"]/div/div/div[3]/button[2]");
            return button;
        }
    }
}
