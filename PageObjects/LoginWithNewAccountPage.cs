﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace WhereIsMyTransportAssessment.PageObjects
{
    class LoginWithNewAccountPage
    {
        public By getUsername()
        {
            By username = By.Id("loginusername");
            return username;
        }

        public By getPassword()
        {
            By password = By.Id("loginpassword");
            return password;
        }

        public By getLoginLink()
        {
            By loginlink = By.Id("login2");
            return loginlink;
        }

        public By getLoginbutton()
        {
            By loginbtn = By.XPath("//*[@id=\"logInModal\"]/div/div/div[3]/button[2]");
            return loginbtn;
        }

        public By getloggedInUsername()
        {
            By loggedinUser = By.Id("nameofuser");
            return loggedinUser;
        }

        public By getlogoutLink()
        {
            By logout = By.Id("logout2");
            return logout;
        }
    }
}
