﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace WhereIsMyTransportAssessment.PageObjects
{
    class AddProduct2CartPage
    {
        public By getSpecificItem()
        {
            By specificproduct = By.XPath("//*[@id=\"tbodyid\"]/div[1]/div/div/h4/a");
            return specificproduct;
        }

        public By getAdd2Cartbtn()
        {
            By add2cartbtn = By.XPath("//*[@id=\"tbodyid\"]/div[2]/div/a");
            return add2cartbtn;
        }

        public By getCartmenu()
        {
            By cartmenu = By.XPath("//*[@id=\"navbarExample\"]/ul/li[4]/a");
            return cartmenu;
        }
    }
}
